@component('mail::message')
# Acativation Email

Berikut Kode OTP xxxx
Silakan Masukan Kode OTP tersebut untuk verifkasi
akun anda.

@component('mail::button', ['url' => ''])
    activation
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
